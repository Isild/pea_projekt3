﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_3_PEA_Jaroslaw_Mierzwa
{
    /// <summary>
    /// Klasa jednej danej używanej do zbierania danych podczas wykonywania algorytmu.
    /// </summary>
    class Dane
    {
        public int koszt, iteracja;
        public long czas;

        public Dane(int k, int i, long c)
        {
            koszt = k;
            iteracja = i;
            czas = c;
        }
    }
}
