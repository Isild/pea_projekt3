﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_3_PEA_Jaroslaw_Mierzwa
{
    class Graph
    {
        public int size;//ilość miast
        public int[,] cityMatrix;//droga do każdego miasta

        /// <summary>
        /// Konstruktor grafu wczytujący dane z pliku w formacie txt.
        /// </summary>
        /// <param name="fileName"> - nazwa pliku, z którego wczytujemy dane.</param>
        /// <param name="g"> - obiekt grafu.</param>
        public Graph(string fileName, Graph g)
        {
            readFromFile(fileName);

            for (int i = 0; i < size; i++) //aby tablica miała na przekątnej -1
                for (int j = 0; j < size; j++)
                    if (i == j)
                        this.cityMatrix[i, j] = -1;
        }

        /// <summary>
        /// Funkcja testująca, do usunięcia
        /// </summary>
        public Graph(string nazwaPliku)
        {
            string nazwa = atspToTextFile(nazwaPliku);
            if (nazwa != null)
            {
                Console.WriteLine("Nazwa pliku txt: " + nazwa);
                readFromFile(nazwa);
            }

            /*if (nazwaPliku.Contains("ftv47"))
            {
                readFromFile("tsp_48");
            }
            else if(nazwaPliku.Contains("ftv170"))
            {
                readFromFile("tsp_171");
            }
            else if(nazwaPliku.Contains("rbg403"))
            {
                readFromFile("tsp_403");//zapytać czy tam jest 403 czy 404 wierzchołki
            }
            else
                readFromFile("tsp_" + nazwaPliku);*/

            //this.drawMatrix();
            Console.ReadKey();
        }

        public Graph(string nazwaPliku, bool txt)
        {
            if (nazwaPliku != null)
            {
                Console.WriteLine("Nazwa pliku txt: " + nazwaPliku);
                readFromFile(nazwaPliku);
            }
        }

        public Graph(Graph g)
        {
            this.size = g.size;
            this.cityMatrix = new int[this.size, this.size];

            for (int i = 0; i < this.size; i++)
                for (int j = 0; j < this.size; j++)
                    if (i == j)
                        this.cityMatrix[i, j] = -1;
                    else
                        this.cityMatrix[i, j] = g.cityMatrix[i, j];
        }

        /// <summary>
        /// Funkcja wczytująca dane z pliku o rozszerzeniu txt.
        /// </summary>
        /// <param name="nazwa">-nazwa pliku, z którego chcemy odczytać dane.</param>
        public void readFromFile(string nazwa)
        {
            string line;
            //string nazwa = "text123";
            int counter = 0;

            if (!nazwa.Contains(".txt"))
            {
                int s = nazwa.Length;
                nazwa = nazwa.Insert(s, ".txt");
            }

            if (File.Exists(nazwa))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@"" + nazwa);
                int a = 0, b = 0;//pozycje wprowadzanych miast w tablicy[a,b]

                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 0) //pierwsze wczytanie, rozmiar
                    {
                        counter++;
                        if (!Int32.TryParse(line, out this.size))
                        {
                            Console.WriteLine("Nie udało się odczytać rozmiaru.");
                            break;
                        }
                        else
                        {
                            //Console.WriteLine("Rozmiar: " + this.size);
                            cityMatrix = new int[size, size];
                        }
                    }
                    else //wczytanie punktów do dynamicznej tablicy
                    {
                        //System.Console.WriteLine(line);//wczytanie jednej lini w formie stringa
                        string[] listOfNumbers = line.Split(' ');
                        int x;

                        foreach (string i in listOfNumbers)
                        {
                            if (Int32.TryParse(i, out x))
                            {
                                if (a == size)
                                {
                                    a = 0;
                                    b++;
                                }

                                //System.Console.WriteLine("wczytane elemeny: " + i + ", " + x + ", dodany w: " + a + " " + b);
                                cityMatrix[b, a] = x;
                                a++;
                            }
                        }
                        counter++;
                    }
                }
                file.Close();
                //System.Console.WriteLine("There were {0} lines.", counter);
            }
            else
                Console.WriteLine("Podany plik nie istnieje, spróbuj ponownie.");
        }

        /// <summary>
        /// Funkcja konwertująca pliki .atsp do formy .txt
        /// </summary>
        public string atspToTextFile(string nazwa)
        {
            string line;
            //string nazwa = "text123";
            int counter = 0;
            //string daneDoPliku = "";

            if (!nazwa.Contains(".atsp"))
            {
                int s = nazwa.Length;
                nazwa = nazwa.Insert(s, ".atsp");
            }

            if (File.Exists(nazwa))
            {
                System.IO.StreamReader file = new System.IO.StreamReader(@"" + nazwa);
                int a = 0, b = 0;//pozycje wprowadzanych miast w tablicy[a,b]

                int rozmiar = 0;
                int szerok;
                int diagonal;
                int odle, odle_max;
                int[,] tablicaOdleglosci = new int[1, 1];

                odle_max = 0;

                while ((line = file.ReadLine()) != null)
                {
                    if (counter == 3)
                    {
                        if (line.Contains("DIMENSION: "))
                        {
                            string[] dane = line.Split(' ');

                            if (!Int32.TryParse(dane[1], out rozmiar))
                            {
                                Console.WriteLine("Nie udało się odczytać rozmiaru.");
                                Console.ReadKey();
                                break;
                            }
                            else
                            {
                                tablicaOdleglosci = new int[rozmiar, rozmiar];
                                //daneDoPliku += dane[1] + "\n";
                                Console.WriteLine("Rozmiar odczytany z pliku: " + rozmiar);
                                Console.ReadKey();
                            }
                        }
                        else
                        {
                            Console.WriteLine("Nie można odczytać rozmiaru tabeli!");
                            Console.ReadKey();
                            break;
                        }
                    }
                    else if (counter > 6 && !line.Contains("EOF"))//tu zaczynają się dane
                    {
                        string[] dane = line.Split(' ');

                        for (int i = 0; i < dane.Length; i++)
                        {
                            if (Int32.TryParse(dane[i], out odle))
                            {
                                //Console.WriteLine(dane[i] + ", po konwersji: " + odle);
                                if (a == b)
                                {
                                    if (a == 0)
                                    {
                                        diagonal = odle;
                                    }

                                    odle = 0;
                                }

                                if (a != rozmiar)
                                    tablicaOdleglosci[a, b] = odle;
                                //daneDoPliku += " " + odle;

                                if (odle > odle_max)
                                    odle_max = odle;

                                b++;
                                if (b % rozmiar == 0)
                                {
                                    a++;
                                    b = 0;
                                    //daneDoPliku += "\n";
                                }
                            }
                        }

                    }
                    counter++;//mówi która jest linijka, potrzebne do zczytania rozmiaru i czytania danych po 6 lini
                }
                file.Close();
                //System.Console.WriteLine("There were {0} lines.", counter);
                //Console.Write(daneDoPliku);

                string nazwaZapisywanegoPliku = "tsp_" + rozmiar + ".txt";

                if (!File.Exists(@"" + nazwaZapisywanegoPliku))
                {
                    StreamWriter sw = File.CreateText(@"" + nazwaZapisywanegoPliku);
                    //Console.WriteLine("max: " + odle_max + ", ilc: " + ileCyfr(odle_max));
                    int szer = ileCyfr(odle_max) + 1;

                    sw.WriteLine(rozmiar);
                    for (int i = 0; i < rozmiar; i++)
                    {
                        for (int j = 0; j < rozmiar; j++)
                        {
                            sw.Write("{0," + szer + "}", tablicaOdleglosci[i, j]);
                        }
                        sw.WriteLine("");
                    }
                    sw.Close();
                    return nazwaZapisywanegoPliku;
                }
                else
                {
                    Console.WriteLine("Plik o takiej nazwie już istnieje.\nJeśli chcesz go usunąć istniejący i zapisać wprowadź 1.\nJeśli chcesz go zapisać pod inną nazwą wprowadź 2 i wprowadź nazwę.");
                    int odp;

                    while (true)
                    {
                        if (Int32.TryParse(Console.ReadLine(), out odp))
                        {
                            string nowaNazwa = "";


                            if (odp == 1)
                            {
                                nowaNazwa = nazwaZapisywanegoPliku;
                            }
                            else if (odp == 2)
                            {
                                nowaNazwa = Console.ReadLine();
                            }

                            if (!nowaNazwa.Contains(".txt"))
                                nowaNazwa += ".txt";

                            StreamWriter sw = File.CreateText(@"" + nowaNazwa);
                            int szer = ileCyfr(odle_max) + 1;

                            sw.WriteLine(rozmiar);
                            for (int i = 0; i < rozmiar; i++)
                            {
                                for (int j = 0; j < rozmiar; j++)
                                {
                                    sw.Write("{0," + szer + "}", tablicaOdleglosci[i, j]);
                                }
                                sw.WriteLine("");
                            }
                            sw.Close();
                            return nowaNazwa;
                        }
                        else
                            Console.WriteLine("Wprowadzono złą opcję.");
                    }
                    Console.ReadLine();
                }
            }
            else
            {
                Console.WriteLine("Podany plik nie istnieje, spróbuj ponownie.");
                return null;
            }
        }

        /// <summary>
        /// Funkcja liczy ile cyfr ma liczba. Wykorzystywana do ustawienia szerokości jakiej zajmują cyfry w pliku .txt
        /// </summary>
        /// <param name="liczba"> - liczba do sprawdzenia.</param>
        /// <returns>il</returns> - ilość cyfr w liczbie
        int ileCyfr(int liczba)
        {
            int il = 0;
            int l = liczba;

            if (l == 0)
                il++;
            else
                while (l > 0)
                {
                    l /= 10;
                    il++;
                }
            return il;
        }

        /// <summary>
        /// Funkcja wyświetlająca wczytaną macierz
        /// </summary>
        public void drawMatrix()
        {
            Console.WriteLine("Tablica miast: ");
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    Console.Write(this.cityMatrix[i, j].ToString() + " ");
                }
                Console.WriteLine();
            }
            //Console.ReadLine();
        }
    }
}
