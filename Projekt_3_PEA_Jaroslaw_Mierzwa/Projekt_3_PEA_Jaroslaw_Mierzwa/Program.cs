﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_3_PEA_Jaroslaw_Mierzwa
{
    class Program
    {
        static void Main(string[] args)
        {
            int odp = 0;
            int N = 2;//ilość powtórzeń testów
            Graph g = null;
            //------warunki stopu
            double organiczenieCzasowe = 15;//sekundy wali
            int maxIteracje = 0;
            //------algorytm
            int wielkoscPopulacji = 1000;
            double wspolczynnikMutacji = 0.01;
            double wspolczynnikKrzyzowania = 0.8;
            double rozmiarTurnieju = 5;
            //------najlepsze rozwiązania
            int naj48 = 1776;
            int naj171 = 2755;
            int naj403 = 2465;
            //------krzyzowania
            int krzyzowanie = 0;
            //------mutacje
            int mutacje = 0;

            while (true)
            {
                Console.Clear();
                Console.WriteLine("f zegara: " + Stopwatch.Frequency);
                Console.WriteLine("Menu główne");
                Console.WriteLine("0.Wyjdź.\n1.Wczytaj plik .atsp.\n2.Wczytaj plik .txt.\n3.Kryteria stopu.\n4.Wielkosć populacji.");
                Console.WriteLine("5.Współczynnik mutacji.\n6.Współczynnik krzyżowania.\n7.Uruchom algorytm.\n8.Testy.\n9.Pokaż wczytaną macierz.");

                if (!Int32.TryParse(Console.ReadLine(), out odp))
                {
                    Console.WriteLine("Wprowadziłeś złą opcje, spróbuj ponownie.");
                    Console.ReadLine();
                }
                else
                {
                    switch (odp)
                    {
                        case 0:
                            System.Diagnostics.Process.GetCurrentProcess().Kill();
                            break;
                        case 1:
                            //atsp
                            string nazwaPliku = "";
                            Console.WriteLine("Podaj nazwę pliku z danymi: ");
                            nazwaPliku = Console.ReadLine();
                            wczytajZpliku(nazwaPliku);
                            break;
                        case 2:
                            //txt
                            string nazwatxt;
                            Console.WriteLine("Podaj nazwe pliku txt:");
                            nazwatxt = Console.ReadLine();
                            g = new Graph(nazwatxt + ".txt", true);
                            break;
                        case 3:
                            //kryt stopu
                            Console.WriteLine("Aktualny czas[sek]: " + organiczenieCzasowe + "\nAktualne max iteracje: " + maxIteracje);
                            Console.WriteLine("Co chcesz zmienić?\n1-Czas(w sekundach)\n2-Maksymalna ilość iteracji");
                            Console.WriteLine("Brak zmian spowoduje uruchomienie domyślnych ustawień. Czasu 2/4/6 min w zależności od wielkości grafu. Maksymalna ilość iteracji rozmiar grafu*10000");
                            string o = "0";
                            o = Console.ReadLine();
                            if (o == "1")
                            {
                                Console.WriteLine("Podaj nowe ograniczenie czasowe: ");
                                Double.TryParse(Console.ReadLine(), out organiczenieCzasowe);
                                //organiczenieCzasowe /= 60;
                            }
                            else if (o == "2")
                            {
                                Console.WriteLine("Podaj nową liczbe maksymalnych iteracji: ");
                                Int32.TryParse(Console.ReadLine(), out maxIteracje);
                            }
                            break;
                        case 4:
                            //wielkosc populacji
                            Console.WriteLine("Aktualna populacja :" + wielkoscPopulacji);
                            Console.WriteLine("Podaj nową populację: ");
                            Int32.TryParse(Console.ReadLine(), out wielkoscPopulacji);
                            break;
                        case 5:
                            //mutacja
                            Console.WriteLine("Aktualny współczynnik mutacji :" + wspolczynnikMutacji);
                            Console.WriteLine("Podaj nowy współczynnik: ");
                            Double.TryParse(Console.ReadLine(), out wspolczynnikMutacji);

                            string om = "0";
                            if (mutacje == 0)
                                Console.WriteLine("Obecna mutacja: insert");
                            else if (mutacje == 1)
                                Console.WriteLine("Obecna mutacja: invert");
                            Console.WriteLine("Czy zmienić?: 1-tak, 0-nie");
                            om = Console.ReadLine();

                            if (om == "1")
                            {
                                Console.WriteLine("0-Insert.\n1-Invert.");
                                Int32.TryParse(Console.ReadLine(), out mutacje);

                                if (mutacje > 1)
                                    mutacje = 0;
                                if (mutacje < 0)
                                    mutacje = 0;
                            }
                            break;
                        case 6:
                            //krzyżowanie
                            Console.WriteLine("Aktualny współczynnik krzyżowania :" + wspolczynnikKrzyzowania);
                            Console.WriteLine("Podaj nowy współczynnik: ");
                            Double.TryParse(Console.ReadLine(), out wspolczynnikKrzyzowania);

                            string ok = "0";
                            if (krzyzowanie == 0)
                                Console.WriteLine("Obecne krzyżowanie: PMX");
                            else if (krzyzowanie == 1)
                                Console.WriteLine("Obecna mutacja: NWOX");
                            Console.WriteLine("Czy zmienić?: 1-tak, 0-nie");
                            ok = Console.ReadLine();

                            if (ok == "1")
                            {
                                Console.WriteLine("0-PMX.\n1-NWOX.");
                                Int32.TryParse(Console.ReadLine(), out krzyzowanie);

                                if (krzyzowanie > 1)
                                    krzyzowanie = 0;
                                if (krzyzowanie < 0)
                                    krzyzowanie = 0;
                            }
                            break;
                        case 7:
                            //uruch algorytm
                            if (g != null)//to ma być
                            {
                                Stopwatch Stoper = new Stopwatch();
                                AlgGenetyczny alg = new AlgGenetyczny(g);//to ma być
                                //AlgGenetyczny alg = new AlgGenetyczny();
                                //rozmiarTurnieju = wielkoscPopulacji*0.1;
                                List<Dane> lista;
                                Stoper.Start();
                                lista = alg.Algorytm(organiczenieCzasowe, maxIteracje, wielkoscPopulacji, wspolczynnikMutacji, wspolczynnikKrzyzowania, rozmiarTurnieju, Stoper, mutacje, krzyzowanie);
                                Stoper.Stop();

                                wynikiDoPliku(lista);//to ma być
                                Console.WriteLine("Czas trwania: " + Stoper.Elapsed);
                            }
                            else
                            {
                                AlgGenetyczny alg = new AlgGenetyczny();
                                Console.WriteLine("Wczytaj graf z pliku.");
                            }
                            Console.ReadLine();
                            break;
                        case 8:
                            //testy
                            Stopwatch zegar = new Stopwatch();
                            zegar.Start();
                            zegar.Stop();

                            Console.ReadLine();
                            break;
                        case 9:
                            if (g != null)
                                g.drawMatrix();
                            else
                                Console.WriteLine("Wczytaj graf z pliku.");
                            Console.ReadKey();
                            break;
                        default:
                            Console.WriteLine("Wybrałeś złą opcję, spróbuj jeszcze raz.");
                            Console.ReadLine();
                            break;
                    }
                }
            }

            /// <summary>
            /// Funkcja wczytująca dane z pliku do grafu, na którym będzie przeprowadzany algorytm.
            /// </summary>
            Graph wczytajZpliku(string nazwaPliku)
            {
                g = new Graph(nazwaPliku);

                Console.ReadLine();
                return g;
            }

            /// <summary>
            /// Funkcja zapisująca dane do pliku.
            /// </summary>
            void wynikiDoPliku(List<Dane> wyniki)
            {
                DateTime dt = DateTime.Now;
                int najlRozw = 0;
                //double blad = 0;

                if (g.size == 48)
                    najlRozw = naj48;
                else if (g.size == 10)
                    najlRozw = 212;
                else if (g.size == 171)
                    najlRozw = naj171;
                else if (g.size == 403)
                    najlRozw = naj403;

                if (!Directory.Exists(@"Wyniki_Testow_Genetycznych__Wykresy"))
                {
                    Directory.CreateDirectory(@"Wyniki_Testow_Genetycznych__Wykresy");
                }
                string nazwaWyniku = "wynik_" + g.size + "_" + String.Format("{0:dd-MM-yy-H;mm;ss}", dt);
                StreamWriter sw = File.CreateText(@"" + "Wyniki_Testow_Genetycznych__Wykresy" + "\\" + nazwaWyniku + ".txt");
                sw.WriteLine("Rozwiązanie dla problemu tsp_" + g.size + ":");//wpisać wyniki po ;
                sw.WriteLine("Populacja ; " + wielkoscPopulacji + " ; Mutacja ; " + wspolczynnikMutacji + " ; Krzyzowanie ; " + wspolczynnikKrzyzowania + " ; Rozmiar Turnieju ; " + rozmiarTurnieju);
                sw.WriteLine("");
                sw.WriteLine("Ograniczenie czasowe ; " + organiczenieCzasowe);

                sw.Write("Koszt ; Błąd[%] ; Iteracja ; Czas[takty zegara] ; Czas[ms] ; Czas [s]");
                long freg = Stopwatch.Frequency;
                foreach (Dane d in wyniki)
                {
                    sw.WriteLine("");
                    int blad = (d.koszt - najlRozw) / najlRozw * 100;
                    if (blad < 0)
                        blad *= -1;
                    double czasMs = d.czas * 1000000000 / freg / 1000000;
                    double czasS = czasMs / 1000;

                    sw.Write(d.koszt + " ; " + blad + " ; " + d.iteracja + " ; " + d.czas + " ; " + czasMs + " ; " + czasS);
                }
                sw.Close();
            }
        }
    }
}
