﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_3_PEA_Jaroslaw_Mierzwa
{
    class AlgGenetyczny
    {
        public static Random rnd = new Random();

        Graph g;
        //usunąć A z końca jeśli się przydadzą jako zmienne globalne
        double ograniczenieCzasowe;
        int maxIteracje;
        int wielkoscPopulacji;
        double wspolczynnikMutacji;
        double wspolczynnikKrzyzowania;
        double rozmiarTurnieju;
        //---------Zmienne do alg 
        long czasZnalez = 0;
        int itZnalez = 0;
        int najlepszyKoszt = Int32.MaxValue;
        int[] najlepszaDroga;
        int[] tmpDrogaKopia;//do zapasu
        int[] tmpDroga;//na niej przeprowadzamy zmiany
        List<int[]> populaxcja;

        public AlgGenetyczny(Graph G)
        {
            g = new Graph(G);
            najlepszaDroga = new int[g.size];
            tmpDrogaKopia = new int[g.size];
            tmpDroga = new int[g.size];
            populaxcja = new List<int[]>();//wiersze to kolejne ścieżki, kolumny to jakieś tam miasta
        }

        public AlgGenetyczny()//do usunięcia
        {
            int[] r1 = { 0, 1, 2, 3, 4, 5, 6, 7, 8 };
            int[] r2 = { 4, 2, 5, 6, 7, 0, 1, 8, 3 };

            Console.WriteLine("Rodzic1:");
            foreach (int x in r1)
                Console.Write(x + " ");
            Console.WriteLine();

            Console.WriteLine("Rodzic2:");
            foreach (int x in r2)
                Console.Write(x + " ");
            Console.WriteLine();

            testKrzyzowania(r1, r2);
            Console.WriteLine("\n---------------------------------------\n");

            Console.WriteLine("Dziecko1:");
            foreach (int x in r1)
                Console.Write(x + " ");
            Console.WriteLine();

            Console.WriteLine("Dziecko2:");
            foreach (int x in r2)
                Console.Write(x + " ");
            Console.WriteLine();
        }

        public void testKrzyzowania(int[] tab1, int[] tab2)
        {
            //PMX(tab1, tab2);
            NWOX(tab1, tab2);
        }

        public List<Dane> Algorytm(double ogCz, int mI, int wP, double wM, double wK, double rT, Stopwatch Stoper, int mutacja, int krzyzowanie)
        {
            List<Dane> wyniki = new List<Dane>();
            //Random rnd = new Random();
            ograniczenieCzasowe = ogCz;
            maxIteracje = mI;
            wielkoscPopulacji = wP;
            wspolczynnikMutacji = wM;
            wspolczynnikKrzyzowania = wK;
            rozmiarTurnieju = rT;
            Stoper.Stop();

            for (int i = 0; i < wielkoscPopulacji; i++) //losowanie populacji startowej
            {
                int[] tab = new int[g.size];
                for (int t = 0; t < g.size; t++)
                    tab[t] = t;

                int[] tmp = generujPermutacje(tab, 0, g.size);
                //algZachlanny(tmp);//uruchomienie alg zachłannego

                shuffle(tmp);

                int w = rnd.Next() % 2;
                if (w == 0)
                    mInsert(tmp);
                else
                    mInvert(tmp);

                if (populaxcja.Contains(tmp))
                    i--;
                else
                    populaxcja.Add(tmp);
                //Console.WriteLine("wylosowany:");
                //foreach (int x in tmp)
                //    Console.Write(x + " ");
                //Console.WriteLine();
            }

            Stoper.Start();

            //pokazPopulacje();//-------------------------------------------Pokaż populację

            //---------------------------Obliczam najlepszy koszt z populacji
            for (int pop = 0; pop < wielkoscPopulacji; pop++)
            {
                int k = obliczKoszt(populaxcja[pop]);
                if (k < najlepszyKoszt)//mniejsze
                {
                    najlepszyKoszt = k;

                    for (int t = 0; t < g.size; t++)
                        najlepszaDroga[t] = populaxcja[pop][t];
                }
            }
            wyniki.Add(new Dane(najlepszyKoszt, itZnalez, Stoper.ElapsedTicks));

            //---------------------------

            while (Stoper.ElapsedMilliseconds <= 1000 * ograniczenieCzasowe)
            {
                itZnalez++;
                List<int[]> nowaPopulacja = new List<int[]>();
                List<int> osobnikiWTurnieju = new List<int>();

                for (int o = 0; o < wielkoscPopulacji; o++)//losuję N osobników do nowej populacji
                {
                    int[] wygrany = null;
                    int najlKoszt = Int32.MaxValue;
                    int r = 0;
                    //turniej, wybieram 5 osobników i z nich najlepszego wybieram
                    for (int i = 0; i < rozmiarTurnieju; i++)
                    {
                        r = rnd.Next(0, wielkoscPopulacji) % wielkoscPopulacji;
                        while (true)//unikam wybrania tych samych osobników do turnieju
                        {
                            if (!osobnikiWTurnieju.Contains(r))
                                break;
                            else
                                r = rnd.Next(0, wielkoscPopulacji) % wielkoscPopulacji;
                        }
                        osobnikiWTurnieju.Add(r);

                        int[] tmp = populaxcja[r];
                        if (obliczKoszt(tmp) < najlKoszt)
                        {
                            wygrany = tmp;
                        }
                    }
                    osobnikiWTurnieju.Clear();
                    nowaPopulacja.Add(wygrany);
                }
                int y = 0;//usunąć

                //bool[] czySkrzyzowane = new bool[wielkoscPopulacji];//mówi czy dane osobniki były krzyżowane
                //for (int b = 0; b < wielkoscPopulacji; b++)
                //    czySkrzyzowane[b] = false;//mozna różne krzyżować

                bool[] czySkrzyzowane = new bool[wielkoscPopulacji];

                int o1 = 0, o2 = 0;
                for (int o = 0; o < wielkoscPopulacji / 2; o++)//wybieram N/2 osobników do krzyżowania
                {
                    while (o1 == o2)
                    {
                        o1 = rnd.Next(0, wielkoscPopulacji) % wielkoscPopulacji;
                        o2 = rnd.Next(0, wielkoscPopulacji) % wielkoscPopulacji;

                        if (czySkrzyzowane[o1] == true || czySkrzyzowane[o2] == true)
                            o1 = o2;//nie mozna różne krzyżować
                    }

                    if (prawdopodob1(wspolczynnikKrzyzowania))//czy poddać krzyżowaniu dwa osobniki
                    {
                        if (krzyzowanie == 0)
                            PMX(nowaPopulacja[o1], nowaPopulacja[o2]);
                        else if (krzyzowanie == 1)
                            NWOX(nowaPopulacja[o1], nowaPopulacja[o2]);

                        if (wspolczynnikMutacji < 0.1)
                        {
                            if (prawdopodob2(wspolczynnikMutacji))
                            {
                                if (mutacja == 0)
                                {
                                    mInsert(nowaPopulacja[o1]);
                                    mInsert(nowaPopulacja[o2]);
                                }
                                else if (mutacja == 1)
                                {
                                    mInvert(nowaPopulacja[o1]);
                                    mInvert(nowaPopulacja[o2]);
                                }
                            }
                        }
                        else
                        {
                            if (prawdopodob1(wspolczynnikMutacji))
                            {
                                if (mutacja == 0)
                                {
                                    mInsert(nowaPopulacja[o1]);
                                    mInsert(nowaPopulacja[o2]);
                                }
                                else if (mutacja == 1)
                                {
                                    mInvert(nowaPopulacja[o1]);
                                    mInvert(nowaPopulacja[o2]);
                                }
                            }
                        }

                    }
                }

                //mam już nową populację, teraz kopiuję ją jako populację wyjściową
                populaxcja.Clear();
                for (int pop = 0; pop < wielkoscPopulacji; pop++)
                    populaxcja.Add(nowaPopulacja[pop]);

                int[] tmpNaj = new int[g.size];
                int tmpKoszt = Int32.MaxValue;
                //szukam najlepszego rozwiązania z populacji
                for (int pop = 0; pop < wielkoscPopulacji; pop++)
                {
                    int k = obliczKoszt(populaxcja[pop]);
                    if (k < tmpKoszt)//mniejsze
                    {
                        tmpKoszt = k;
                        for (int t = 0; t < g.size; t++)
                            tmpNaj[t] = populaxcja[pop][t];
                    }
                }

                //pokazPopulacje();

                //wyniki.Add(new Dane(tmpKoszt, itZnalez, Stoper.ElapsedTicks));
                if (tmpKoszt < najlepszyKoszt)//sprawdzam czy może to być najlepsze rozwiązanie
                {
                    najlepszyKoszt = tmpKoszt;
                    for (int t = 0; t < g.size; t++)
                        najlepszaDroga[t] = tmpNaj[t];

                    czasZnalez = Stoper.ElapsedTicks;

                    //wyniki.Add(new Dane(najlepszyKoszt, itZnalez, czasZnalez));
                }
                wyniki.Add(new Dane(najlepszyKoszt, itZnalez, Stoper.ElapsedTicks));
            }
            pokazRozw(najlepszaDroga);
            return wyniki;
        }

        public void pokazPopulacje()
        {
            Console.WriteLine("\nPopulacja:");
            foreach (int[] x in populaxcja)
            {
                for (int i = 0; i < x.Length; i++)
                    Console.Write(x[i] + " ");
                Console.WriteLine();
            }
            Console.WriteLine();
        }

        /// <summary>
        /// Prawdopodobieństwo jedno miejsce po przecinku.
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool prawdopodob1(double x)
        {
            //Random rnd = new Random();
            double los = (double)((double)rnd.Next(0, 10) / 10.0);

            if (los <= x)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Prawdopodobieństwo, dwa miejsca po przecinku
        /// </summary>
        /// <param name="x"></param>
        /// <returns></returns>
        public bool prawdopodob2(double x)
        {
            //Random rnd = new Random();
            double los = (double)((double)rnd.Next(0, 100) / 100.0);

            if (los <= x)
                return true;
            else
                return false;
        }

        /// <summary>
        /// Krzyżowanie PMX.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        public void PMX(int[] r1, int[] r2)
        {
            // Random rnd = new Random();
            //int c1 = rnd.Next(0, r1.Length - 1);//losuję miejsca w których podzielę rodziców na 3 części
            //int c2 = rnd.Next(0, r1.Length - 1);
            int c1 = rnd.Next() % r1.Length;//losuję miejsca w których podzielę rodziców na 3 części
            int c2 = rnd.Next() % r1.Length;

            while (c1 == c2)//unikam przypadku że są takie same
                c2 = rnd.Next(0, r1.Length - 1);


            if (c1 > c2)//c1 zawsze ma być mniejsze niż c2
            {
                int z = c1;
                c1 = c2;
                c2 = z;
            }

            //c1 = 2;//zamieszanie
            //c2 = 6;

            int rozmMap = c2 - c1;//rozmiar mapy przejść
            int[,] mapPrzej = new int[2, rozmMap];

            for (int i = c1; i < c2; i++)//mapa przejść
            {
                mapPrzej[0, i - c1] = r1[i];
                mapPrzej[1, i - c1] = r2[i];
            }
            int[] kr1 = new int[r1.Length];//kopie rodziców, bo na rodzicach będą dokonywane krzyżowania i będą dziećmi 
            int[] kr2 = new int[r1.Length];
            for (int i = 0; i < r1.Length; i++)
            {
                kr1[i] = r1[i];
                kr2[i] = r2[i];
            }

            bool[,] czySr = new bool[2, r1.Length];//mówi czy wierzchołki były już użyte
            for (int i = 0; i < r1.Length; i++)
            {
                czySr[0, i] = false;//dziecko 1
                czySr[1, i] = false;//dziecko 2
            }

            for (int i = c1; i < c2; i++)//kopiowanie części środkowych
            {
                r1[i] = kr2[i];
                czySr[0, r1[i]] = true;

                r2[i] = kr1[i];
                czySr[1, r2[i]] = true;
            }

            ////////////////////

            kopiaRD(r1, kr1, mapPrzej, czySr, c1, c2, 0);
            kopiaRD(r2, kr2, mapPrzej, czySr, c1, c2, 1);

        }

        /// <summary>
        /// Krzyżowanie NWOX.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="r2"></param>
        public void NWOX(int[] r1, int[] r2)
        {
            //Random rnd = new Random();
            //int c1 = rnd.Next(0, r1.Length - 1);//losuję miejsca w których podzielę rodziców na 3 części
            //int c2 = rnd.Next(0, r1.Length - 1);

            int c1 = rnd.Next() % r1.Length;//losuję miejsca w których podzielę rodziców na 3 części
            int c2 = rnd.Next() % r1.Length;

            while (c1 == c2)//unikam przypadku że są takie same
                c2 = rnd.Next(1, r1.Length - 2);

            if (c1 > c2)//c1 zawsze ma być mniejsze niż c2
            {
                int z = c1;
                c1 = c2;
                c2 = z;
            }

            //c1 = 2;//zamieszanie
            //c2 = 6;

            int rozmMap = c2 - c1;//rozmiar mapy przejść
            int[,] mapPrzej = new int[2, rozmMap];

            for (int i = c1; i < c2; i++)//mapa przejść
            {
                mapPrzej[0, i - c1] = r1[i];
                mapPrzej[1, i - c1] = r2[i];
            }

            int[] kr1 = new int[r1.Length];//kopie rodziców, bo na rodzicach będą dokonywane krzyżowania i będą dziećmi 
            int[] kr2 = new int[r1.Length];
            for (int i = 0; i < r1.Length; i++)
            {
                kr1[i] = r1[i];
                kr2[i] = r2[i];
            }

            bool[,] czySr = new bool[2, r1.Length];//mówi czy wierzchołki są w środkach przeciwnych rodziców
            for (int i = 0; i < r1.Length; i++)
            {
                czySr[0, i] = false;//dziecko 1
                czySr[1, i] = false;//dziecko 2
            }

            for (int i = c1; i < c2; i++)//zaznaczam które wierzchołki z części śrotkowych są 
            {
                czySr[0, r1[i]] = true;
                czySr[1, r2[i]] = true;
            }

            bool[,] czyKonflikt = new bool[2, r1.Length];//mówi czy wierzchołki są w konflikcie 
            for (int i = 0; i < r1.Length; i++)
            {
                czyKonflikt[0, i] = false;//dziecko 1
                czyKonflikt[1, i] = false;//dziecko 2

                if (czySr[1, i] == true)
                    czyKonflikt[0, i] = true;

                if (czySr[0, i] == true)
                    czyKonflikt[1, i] = true;
            }

            //przesówam liczby na boki
            przesuniecieNWOX(r1, czyKonflikt, 0, c1, c2);
            przesuniecieNWOX(r2, czyKonflikt, 1, c1, c2);

            for (int i = c1; i < c2; i++)//kopiowanie części środkowych
            {
                r1[i] = kr2[i];
                r2[i] = kr1[i];
            }
        }

        /// <summary>
        /// Przesunięcie liczb na boki w algorytmie NWOX.
        /// </summary>
        /// <param name="d"></param>
        /// <param name="czyKonflikt"></param>
        /// <param name="nr"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        public void przesuniecieNWOX(int[] d, bool[,] czyKonflikt, int nr, int c1, int c2)
        {
            for (int i = 0; i < c1; i++)//przesówam pierwszą część do lewej
            {
                if (czyKonflikt[nr, d[i]] == true) //jeżeli jest konflikt to przesówam c1 liczb, które nie są konfliktem 
                {
                    int sw = i;//szukam gdzie jest liczba bez konfliktu
                    bool war = true;
                    while (war)
                    {
                        sw++;
                        if (czyKonflikt[nr, d[sw]] == false)
                            war = false;
                    }
                    czyKonflikt[nr, d[sw]] = true;
                    czyKonflikt[nr, d[i]] = false;
                    d[i] = d[sw];
                }
            }

            for (int i = d.Length - 1; i >= c2; i--)//przesówam prawą część
            {
                if (czyKonflikt[nr, d[i]] == true) //jeżeli jest konflikt to przesówam c1 liczb, które nie są konfliktem 
                {
                    int sw = i;//szukam gdzie jest liczba bez konfliktu
                    bool war = true;
                    while (war)
                    {
                        sw--;
                        if (czyKonflikt[nr, d[sw]] == false)
                            war = false;
                    }
                    czyKonflikt[nr, d[sw]] = true;
                    czyKonflikt[nr, d[i]] = false;
                    d[i] = d[sw];
                }
            }
        }

        /// <summary>
        /// Funkcja przekopiowująca boki dzieci z rodziców.
        /// </summary>
        /// <param name="r1"></param>
        /// <param name="kr1"></param>
        /// <param name="mapPrzej"></param>
        /// <param name="czySr"></param>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <param name="nrDziecka"></param>
        public void kopiaRD(int[] r1, int[] kr1, int[,] mapPrzej, bool[,] czySr, int c1, int c2, int nrDziecka)
        {
            //jak coś to ustawić i=1, żeby nie ruszać pierwszego wierzchołka
            for (int i = 0; i < c1; i++)//część lewa
            {
                if (czySr[nrDziecka, kr1[i]] == false)//jeśli nie ma konfliktu
                {
                    r1[i] = kr1[i];//przypisujemy wartości z rodzica 1 do dziecka 1
                    czySr[nrDziecka, r1[i]] = true;
                }
                else//idziemy do mapy przejść
                {
                    bool[] odw = new bool[mapPrzej.Length / 2];//mówi które przejścia użyte podczas jednej ścieżki
                    for (int a = 0; a < mapPrzej.Length / 2; a++)
                        odw[a] = false;

                    int przejscie = kr1[i];
                    bool war = true;

                    while (war)
                    {
                        //bierzemy pierwszą wartość
                        //przeszukanie góry
                        int it = 0;
                        bool czyGora = false;//sprawdzam gdzie na mapie znaleziono liczba

                        for (it = 0; it < mapPrzej.Length / 2; it++)
                        {
                            if (mapPrzej[0, it] == przejscie && odw[it] == false)
                            {
                                odw[it] = true;
                                czyGora = true;
                                break;//znajduję gdzie jest przejście
                            }
                        }
                        if (czyGora == false)
                        {
                            for (it = 0; it < mapPrzej.Length / 2; it++)
                            {
                                if (mapPrzej[1, it] == przejscie && odw[it] == false)
                                {
                                    odw[it] = true;
                                    czyGora = false;
                                    break;//znajduję gdzie jest przejście
                                }
                            }
                        }

                        if (czyGora)
                        {
                            //it--;
                            przejscie = mapPrzej[1, it];

                            if (czySr[nrDziecka, przejscie] == false)
                            {
                                //Console.WriteLine("-----Znaleziome: " + przejscie);
                                r1[i] = przejscie;//przypisujemy wartości z rodzica 1 do dziecka 1
                                czySr[nrDziecka, r1[i]] = true;
                                war = false;
                            }
                            else
                            {
                                //Console.WriteLine(mapPrzej[1, it] + " ");
                                przejscie = mapPrzej[1, it];
                            }
                        }
                        else
                        {
                            //it--;
                            przejscie = mapPrzej[0, it];

                            if (czySr[nrDziecka, przejscie] == false)
                            {
                                //Console.WriteLine("-----Znaleziome: " + przejscie);
                                r1[i] = przejscie;//przypisujemy wartości z rodzica 1 do dziecka 1
                                czySr[nrDziecka, r1[i]] = true;
                                war = false;
                            }
                            else
                            {
                                //Console.WriteLine(mapPrzej[0, it] + " ");
                                przejscie = mapPrzej[0, it];
                            }
                        }
                    }
                }
            }

            //Console.WriteLine("boole");
            //for (int i = 0; i < r1.Length; i++)
            //    Console.WriteLine("i: " + i + ", " + czySr[nrDziecka, i]);

            for (int i = c2; i < r1.Length; i++)//część prawa 
            {
                if (czySr[nrDziecka, kr1[i]] == false)//jeśli nie ma konfliktu
                {
                    r1[i] = kr1[i];//przypisujemy wartości z rodzica 1 do dziecka 1
                    czySr[nrDziecka, r1[i]] = true;
                }
                else//idziemy do mapy przejść
                {
                    bool[] odw = new bool[mapPrzej.Length / 2];//mówi które przejścia użyte podczas jednej ścieżki
                    for (int a = 0; a < mapPrzej.Length / 2; a++)
                        odw[a] = false;

                    int przejscie = kr1[i];
                    bool war = true;

                    while (war)
                    {
                        //bierzemy pierwszą wartość
                        //przeszukanie góry
                        int it = 0;
                        bool czyGora = false;//sprawdzam gdzie na mapie znaleziono liczba
                        for (it = 0; it < mapPrzej.Length / 2; it++)
                        {
                            if (mapPrzej[0, it] == przejscie && odw[it] == false)
                            {
                                odw[it] = true;
                                czyGora = true;
                                break;//znajduję gdzie jest przejście
                            }
                            else if (mapPrzej[1, it] == przejscie && odw[it] == false)
                            {
                                odw[it] = true;
                                czyGora = false;
                                break;//znajduję gdzie jest przejście
                            }
                        }

                        if (czyGora)
                        {
                            //it--;
                            przejscie = mapPrzej[1, it];

                            if (czySr[nrDziecka, przejscie] == false)
                            {
                                //Console.WriteLine("-----Znaleziome: " + przejscie);
                                r1[i] = przejscie;//przypisujemy wartości z rodzica 1 do dziecka 1
                                czySr[nrDziecka, r1[i]] = true;
                                war = false;
                            }
                            else
                            {
                                //Console.WriteLine(mapPrzej[1, it] + " ");
                                przejscie = mapPrzej[1, it];
                            }
                        }
                        else
                        {
                            //it--;
                            przejscie = mapPrzej[0, it];

                            if (czySr[nrDziecka, przejscie] == false)
                            {
                                //Console.WriteLine("-----Znaleziome: " + przejscie);
                                r1[i] = przejscie;//przypisujemy wartości z rodzica 1 do dziecka 1
                                czySr[nrDziecka, r1[i]] = true;
                                war = false;
                            }
                            else
                            {
                                //Console.WriteLine(mapPrzej[0, it] + " ");
                                przejscie = mapPrzej[0, it];
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Funkcja dokonująca zamiany Insert.
        /// </summary>
        /// <param name="perm"> - tablica, na której zostanie dokonana zamiana.</param>
        public void mInsert(int[] perm)
        {
            //Random r = new Random();
            int i;
            int j;

            do
            {
                i = rnd.Next() % g.size;
                j = rnd.Next() % g.size;
            } while (i == j);

            if (i > j)
            {
                int zap = perm[i];

                for (int a = i; a > j; a--)
                {
                    perm[a] = perm[a - 1];
                }

                perm[j + 1] = perm[j];
                perm[j] = zap;
            }
            else
            {
                int zap = perm[j];

                for (int a = j; a > i; a--)
                {
                    perm[a] = perm[a - 1];
                }

                perm[i + 1] = perm[i];
                perm[i] = zap;
            }
        }

        /// <summary>
        /// Funkcja dokonująca zamiany Swap.
        /// </summary>
        /// <param name="perm"> - tablica, na której zostanie dokonana zamiana.</param>
        public void mSwap(int[] perm)
        {
            //Random r = new Random();
            int i;
            int j;

            do
            {
                i = rnd.Next() % g.size;
                j = rnd.Next() % g.size;
            } while (i == j);

            int zap = perm[i];

            perm[i] = perm[j];
            perm[j] = zap;
        }

        /// <summary>
        /// Funkcja dokonująca zamiany Invert.
        /// </summary>
        /// <param name="perm"> - tablica, na której zostanie dokonana zamiana.</param>
        public void mInvert(int[] perm)
        {
            //Random r = new Random();
            int i;
            int j;

            do
            {
                i = rnd.Next() % g.size;
                j = rnd.Next() % g.size;
            } while (i == j);

            int z = i;
            if (i > j)
            {
                i = j;
                j = z;
            }

            for (int a = 0; a < (j - i) / 2; a++)
            {
                z = perm[i + a];
                perm[i + a] = perm[j - a];
                perm[j - a] = z;
            }
        }


        //usun
        void genPerm(int[] tab, int n, int i)
        {
            int c;
            if (i >= n - 1)
                c = 0;
            else
            {
                genPerm(tab, n, i + 1);
                for (int j = i + 1; j < n; j++)
                {
                    int z = 0;
                    z = tab[j];
                    tab[j] = tab[i];
                    tab[i] = z; ;

                    genPerm(tab, n, i + 1);

                    z = tab[j];
                    tab[j] = tab[i];
                    tab[i] = z;
                }
            }
        }

        void shuffle(int[] list)
        {
            //Random rng = new Random();

            int n = list.Length;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(n + 1);
                int value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Funkcja wykonująca losową permutacje.
        /// </summary>
        /// <param name="perm"></param>
        /// <param name="begin"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        int[] generujPermutacje(int[] perm, int begin, int end)
        {
            //Random rnd = new Random();

            bool[] tab = new bool[g.size];//tablica mówiąca które elementy zostały poddane permutacji
            for (int i = 0; i < g.size; i++)
            {
                if (i >= begin && i <= end)//elementy które zostaną poddane permutacji
                    tab[i] = false;
                else
                    tab[i] = true;

                perm[i] = i;
                //perm[i] = (i + r.Next()) % g.size;
                //perm[i] = r.Next() % wielkoscPopulacji;
            }

            //-----------------------------------------
            //int z = 0;
            //if (r.Next() % 2 == 0)
            //    for (int i = g.size - 1; i >= 0; i++)
            //    {
            //        z = perm[i];
            //        perm[i] = perm[g.size - 1 - i];
            //        perm[g.size - 1 - i] = z;
            //    }

            // shuffle(perm);
            //genPerm(perm, perm.Length, 0);

            int randomIndex = 0;
            int[] retT = new int[perm.Length];
            int it = 0;//iterator mówiący w którym się jest miejscu w nowej tablicy

            //for przekopiowujący to co jest przed miejscem permutacji
            for (int i = 0; i < begin; i++)
            {
                retT[it] = perm[i];
                it++;
            }

            int il = end - begin;//tyle losowań trzeba zrobić
            while (il > 0)
            {
                randomIndex = rnd.Next(begin, end);
                if (tab[randomIndex] == false)
                {
                    retT[it] = perm[randomIndex];
                    it++;
                    tab[randomIndex] = true;
                    il--;
                }
            }

            for (int i = 0; i < g.size; i++)
                mInsert(perm);

            //for przekopiowujący to co jest za miejscem permutacji
            for (int i = end; i < g.size; i++)
            {
                retT[it] = perm[i];
                it++;
            }

            return retT;
        }

        /// <summary>
        /// Algorytm zachłanny.
        /// </summary>
        /// <param name="pierwszaPermu"></param>
        public void algZachlanny(int[] pierwszaPermu)
        {
            pierwszaPermu = generujPermutacje(pierwszaPermu, 0, pierwszaPermu.Length);//losowa generacja drogi
            bool locallyOptimal = false;
            int koszt = obliczKoszt(pierwszaPermu);//aktualny koszt scieżki

            int[] x = new int[g.size];//najmiejsze x w sąsiedztwie x0
            int kosztX = Int32.MaxValue;
            int[] tmp = new int[g.size];//dana permutacja z sąsiedztwa
            int kosztTmp = koszt;

            do
            {
                for (int i = 0; i < 100; i++)//szukam w sąsiedztwie takiego x dla którego f(x) jest najmniejsze
                {
                    for (int a = 0; a < g.size; a++)//za każdym razem permutuję podaną permutację
                        tmp[a] = pierwszaPermu[a];

                    //tmp = mInsert(tmp);
                    mInsert(tmp);//biorę permutacje ze zbioru sąsiedztwa
                    kosztTmp = obliczKoszt(tmp);

                    if (kosztTmp < kosztX)
                    {
                        kosztX = kosztTmp;

                        for (int a = 0; a < g.size; a++)
                            x[a] = tmp[a];
                    }
                }

                if (koszt > kosztX)
                {
                    koszt = kosztX;
                    for (int a = 0; a < g.size; a++)
                        pierwszaPermu[a] = x[a];
                }
                else
                    locallyOptimal = true;

            } while (locallyOptimal == false);
            //return pierwszaPermu;
        }

        /// <summary>
        /// Funkcja obliczająca koszt drogi.
        /// </summary>
        /// <param name="droga"></param>
        /// <returns>Koszt drogi.</returns>
        int obliczKoszt(int[] droga)
        {
            int koszt = 0;

            for (int i = 1; i < g.size; i++)
            {
                koszt += g.cityMatrix[droga[i - 1], droga[i]];
            }
            koszt += g.cityMatrix[g.size - 1, 0];

            return koszt;
        }

        /// <summary>
        /// Funkcja zapisująca wynik drogi do pliku, zwraca koszt drogi.
        /// </summary>
        /// <param name="droga"></param>
        /// <returns></returns>
        int pokazRozw(int[] droga)
        {
            long freg = Stopwatch.Frequency;

            Console.WriteLine("Rozwiązanie:");
            Console.WriteLine("Czas znalezienia[ms]: " + czasZnalez * 1000000000 / freg / 1000000);
            Console.WriteLine("Koszt drogi wynosi: " + obliczKoszt(droga));
            string sciezka = "";

            foreach (int c in droga)
            {
                Console.Write(c + " - ");
                sciezka += c + " - ";
            }
            Console.WriteLine(droga[0]);
            sciezka += droga[0];

            //zapis do pliku rozwiązania
            DateTime dt = DateTime.Now;

            if (!Directory.Exists(@"Wyniki_" + String.Format("{0:dd-MM-yy-H}", dt)))
            {
                Directory.CreateDirectory(@"Wyniki_" + String.Format("{0:dd-MM-yy-H}", dt));
            }

            string nazwaWyniku = "wynikTS_" + g.size + "_" + String.Format("{0:dd-MM-yy-H;mm;ss}", dt);
            StreamWriter sw = File.CreateText(@"" + "Wyniki_" + String.Format("{0:dd-MM-yy-H}", dt) + "\\" + nazwaWyniku + ".txt");

            sw.WriteLine("Rozwiązanie dla problemu tsp_" + g.size + ":");
            sw.WriteLine("Scieżka: " + sciezka);
            sw.WriteLine("Koszt: " + obliczKoszt(droga));
            sw.WriteLine("Czas znalezienia[takty]: " + czasZnalez);
            sw.WriteLine("Czas znalezienia[ms]: " + czasZnalez * 1000000000 / freg / 1000000);
            sw.WriteLine("Czas znalezienia[s]: " + czasZnalez * 1000000000 / freg / 1000000 / 1000);
            sw.WriteLine("Iteracja znalezienia: " + itZnalez);

            double rozw10 = 212;
            double rozw48 = 1776;
            double rozw171 = 2755;
            double rozw403 = 2465;
            double wyn = 0;
            double rozw = obliczKoszt(droga);

            if (g.size == 48)
            {
                wyn = (double)(Math.Abs(rozw - rozw48) / rozw48) * 100;
            }
            else if (g.size == 171)
            {
                wyn = (double)(Math.Abs(rozw - rozw171) / rozw171) * 100;
            }
            else if (g.size == 403)
            {
                wyn = (double)(Math.Abs(rozw - rozw403) / rozw403) * 100;
            }
            else if (g.size == 10)
            {
                wyn = (double)(Math.Abs(rozw - rozw10) / rozw10) * 100;
            }
            sw.WriteLine("Błąd względny = " + wyn);
            //Console.WriteLine("Błąd względny = " + wyn);

            sw.Close();
            return obliczKoszt(droga);
        }
    }
}
