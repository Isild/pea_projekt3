\contentsline {section}{\numberline {1}Cel Projektu}{2}
\contentsline {section}{\numberline {2}Wst\IeC {\k e}p teoretyczny}{2}
\contentsline {section}{\numberline {3}Algorytm genetyczny}{2}
\contentsline {subsection}{\numberline {3.1}Funkcja przystosowania}{2}
\contentsline {subsection}{\numberline {3.2}Selekcja}{2}
\contentsline {subsection}{\numberline {3.3}Krzy\IeC {\.z}owanie}{2}
\contentsline {subsubsection}{\numberline {3.3.1}Partially-Mapped Crossover}{2}
\contentsline {subsubsection}{\numberline {3.3.2}Non-Wrapping Order Crossover}{3}
\contentsline {subsection}{\numberline {3.4}Mutacja}{3}
\contentsline {subsection}{\numberline {3.5}Opis programu}{4}
\contentsline {section}{\numberline {4}Wyniki}{5}
\contentsline {subsubsection}{\numberline {4.0.1}Tsp 48}{5}
\contentsline {subsubsection}{\numberline {4.0.2}Tsp 171}{8}
\contentsline {subsubsection}{\numberline {4.0.3}Tsp 403}{11}
\contentsline {section}{\numberline {5}Wnioski}{14}
\contentsline {subsection}{\numberline {5.1}Najlepsze rozwi\IeC {\k a}zania}{16}
\contentsline {subsection}{\numberline {5.2}Tsp 48}{16}
\contentsline {subsection}{\numberline {5.3}Tsp 171}{16}
\contentsline {subsection}{\numberline {5.4}Tsp 403}{16}
\contentsline {section}{\numberline {6}Materia\IeC {\l }y}{17}
